﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;

class WaypointListener : NetworkBehaviour
{
    [SyncVar]
    public float DistancePoint;

    [SyncVar]
    public GameObject LastWaypoint;

    [ServerCallback]
    void Awake()
    {
        DistancePoint = 0;
        LastWaypoint = GameObject.Find("Waypoint 0");
    }

    // Update is called once per frame
    [ServerCallback]
    private void Update()
    {
        if (LastWaypoint != null)
        {
            float sign = Mathf.Sign(transform.position.x - LastWaypoint.transform.position.x);
            DistancePoint = sign * Vector3.Distance(transform.position, LastWaypoint.transform.position);
        }
    }

    [ServerCallback]
    void OnTriggerEnter2D(Collider2D other)
    {
        LastWaypoint = other.gameObject;
    }

    [ServerCallback]
    void OnBecameInvisible()
    {
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        if (players.Length > 1)
        {
            float minDistance = players.Select(x => x.GetComponent<WaypointListener>().DistancePoint).Min();
            float minWaypoint = players.Select(x => int.Parse(
                                                        x.GetComponent<WaypointListener>().LastWaypoint.name
                                                        .Replace("Waypoint ", ""))
                                                        ).Min();

            if (DistancePoint == minDistance && LastWaypoint.name == ("Waypoint " + minWaypoint))
            {
                RpcDestroyPlayer();
            }
        }
    }

    [ClientRpc]
    void RpcDestroyPlayer()
    {
        GameObject.Destroy(gameObject);
    }
}
